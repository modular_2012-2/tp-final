package modular.meutime.feed;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.Serializable;
import java.util.List;

import modular.meutime.bean.RssBean;
import modular.meutime.service.BaseService;
import modular.meutime.utils.MeuTimeConstants;
import modular.meutime.parser.NewsParser;

/**
 * Serviço para recuperar e exibir o Feed de noticia.
 */

public class RssService extends BaseService {

  public static final String ITEMS = "rssItems";
  public static final String RECEIVER = "receiver";

  public RssService() {
    super("RssService");
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    Log.d("MeuTime", "RSSService started");
    List<RssBean> rssItems = null;
    try {
      NewsParser parser = new NewsParser();
      rssItems = parser.parse(getInputStream(getPreferenceClub()));
    } catch (Exception e) {
      Log.w(e.getMessage(), e);
    }
    Bundle bundle = new Bundle();
    bundle.putSerializable(ITEMS, (Serializable) rssItems);
    ResultReceiver receiver = intent.getParcelableExtra(RECEIVER);
    receiver.send(0, bundle);
  }

  private String getPreferenceClub() {
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
    return preferences.getString(MeuTimeConstants.CURRENT_CLUB_RSS, MeuTimeConstants.FeedURL.CRUZEIRO);
  }

}

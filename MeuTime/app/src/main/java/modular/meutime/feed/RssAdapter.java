package modular.meutime.feed;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import modular.meutime.R;
import modular.meutime.bean.RssBean;

/**
 * Adaptador para o Rss.
 */

public class RssAdapter extends BaseAdapter {

  private final List<RssBean> items;
  private final Context context;

  public RssAdapter(List<RssBean> items, Context context) {
    this.items = items;
    this.context = context;
  }

  @Override
  public int getCount() {
    return this.items.size();
  }

  @Override
  public Object getItem(int position) {
    return items.get(position);
  }

  @Override
  public long getItemId(int id) {
    return id;
  }

  @Override
  public View getView(int position, View view, ViewGroup parent) {
    ViewHolder holder = new ViewHolder();
    if (view == null) {
      assert view != null;
      view = View.inflate(context, R.layout.rss_item, null);
      holder.itemTitle = (TextView) view.findViewById(R.id.itemTitle);
      holder.itemDescription = (TextView) view.findViewById(R.id.itemDescription);
      view.setTag(holder);
    }
    if(holder.itemTitle != null && holder.itemDescription != null) {
      holder.itemTitle.setText(items.get(position).getTitle());
      holder.itemDescription.setText(items.get(position).getDescription());
    }
    return view;
  }

  static class ViewHolder {
    TextView itemTitle;
    TextView itemDescription;
//    TextView itemDate;
  }
}

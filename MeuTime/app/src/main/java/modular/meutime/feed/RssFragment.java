package modular.meutime.feed;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import modular.meutime.R;
import modular.meutime.bean.RssBean;

/**
 * Classe que gerencia o fragment do feed.
 */
public class RssFragment extends Fragment implements AdapterView.OnItemClickListener {

  private ProgressBar progressBar;
  private ListView listView;
  private View view;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    if (view == null) {
      view = inflater.inflate(R.layout.list_fragment_layout, container, false);
    } else {
      ViewGroup parent = (ViewGroup) view.getParent();
      parent.removeView(view);
    }
    progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
    this.listView = (ListView) view.findViewById(R.id.listView);
    this.listView.setOnItemClickListener(this);
    startService();
    return view;
  }

  private void startService() {
    Intent intent = new Intent(getActivity(), RssService.class);
    intent.putExtra(RssService.RECEIVER, resultReceiver);
    getActivity().startService(intent);
  }

  /**
   * Recebe o resultado do RSSService
   */
  private final ResultReceiver resultReceiver = new ResultReceiver(new Handler()) {
    @SuppressWarnings("unchecked")
    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      List<RssBean> items = (List<RssBean>) resultData.getSerializable(RssService.ITEMS);
      if (items != null) {
        RssAdapter adapter = new RssAdapter(items, getActivity());
        listView.setAdapter(adapter);
      } else {
        Toast.makeText(getActivity(), "Erro ao recuperar as notícias.", Toast.LENGTH_LONG).show();
      }
      // Remove o
      progressBar.setVisibility(View.GONE);
      listView.setVisibility(View.VISIBLE);
    }
  };

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    RssAdapter adapter = (RssAdapter) parent.getAdapter();
    RssBean item = (RssBean) adapter.getItem(position);
    Uri uri = Uri.parse(item.getLink());
    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
    startActivity(intent);
  }
}

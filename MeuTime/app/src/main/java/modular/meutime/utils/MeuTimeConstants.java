package modular.meutime.utils;

/**
 * Constantes do MeuTime.
 */

public final class MeuTimeConstants {
  public static final String TABLE_API_PATH = "http://jsuol.com.br/c/monaco/utils/gestor/commons.js?callback=simulador_dados_jsonp&file=commons.uol.com.br/sistemas/esporte/modalidades/futebol/campeonatos/dados/2016/30/dados.json";
  public static final String CURRENT_CLUB_RSS = "currentClubRss";
  public static final String CURRENT_CLUB = "currentClub";


  public class FeedURL {
    public static final String ATLETICO_MG = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/atletico-mg/feed.rss";
    public static final String ATLETICO_PR = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/atletico-pr/feed.rss";
    public static final String BOTAFOGO = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/botafogo/feed.rss";
    public static final String CORINTHIANS = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/corinthians/feed.rss";
    public static final String CORITIBA = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/coritiba/feed.rss";
    public static final String CRUZEIRO = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/cruzeiro/feed.rss";
    public static final String FIGUEIRENSE = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/figueirense/feed.rss";
    public static final String FLAMENGO = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/flamengo/feed.rss";
    public static final String FLUMINENSE = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/fluminense/feed.rss";
    public static final String GREMIO = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/gremio/feed.rss";
    public static final String INTERNACIONAL = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/internacional/feed.rss";
    public static final String PALMEIRAS = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/palmeiras/feed.rss";
    public static final String SANTA_CRUZ = "http://globoesporte.globo.com/Esportes/Rss/0,,AS0-10073,00.xml";
    public static final String SANTOS = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/santos/feed.rss";
    public static final String SPORT = "http://globoesporte.globo.com/Esportes/Rss/0,,AS0-9876,00.xml";
    public static final String SAO_PAULO = "http://globoesporte.globo.com/servico/semantica/editorias/plantao/futebol/times/sao-paulo/feed.rss";
    public static final String VITORIA = "http://globoesporte.globo.com/Esportes/Rss/0,,AS0-10072,00.xml";
  }
  public class ClubName {
    public static final String ATLETICO_MG = "atleticomg";
    public static final String ATLETICO_PR = "atleticopr";
    public static final String BOTAFOGO = "botafogo";
    public static final String CORINTHIANS = "corinthians";
    public static final String CORITIBA = "coritiba";
    public static final String CRUZEIRO = "cruzeiro";
    public static final String FIGUEIRENSE = "figueirense";
    public static final String FLAMENGO = "flamengo";
    public static final String FLUMINENSE = "fluminense";
    public static final String GREMIO = "gremio";
    public static final String INTERNACIONAL = "internacional";
    public static final String PALMEIRAS = "palmeiras";
    public static final String SANTA_CRUZ = "santacruz";
    public static final String SANTOS = "santos";
    public static final String SPORT = "sport";
    public static final String SAO_PAULO = "saopaulo";
    public static final String VITORIA = "vitoria";
  }

  public class FeedTag {
    public static final String LINK = "link";
    public static final String DESCRIPTION = "description";
    public static final String TITLE = "title";
    public static final String PUBLISH_DATE = "pubDate";
//    public static final String = "";
  }
}

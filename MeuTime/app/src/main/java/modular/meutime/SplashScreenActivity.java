package modular.meutime;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Atividade da tela de apresentação.
 */
public class SplashScreenActivity extends Activity {
  // Tempo de exibiçao da tela
  private static int SPLASH_TIME_OUT = 1500;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        // Inicia a MainActivity
        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
        startActivity(i);

        finish();
      }
    }, SPLASH_TIME_OUT);
  }
}

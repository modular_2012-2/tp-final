package modular.meutime;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import modular.meutime.feed.RssFragment;
import modular.meutime.table.TableFragment;
import modular.meutime.utils.MeuTimeConstants;

/**
 * Atividade principal.
 */
public class MainActivity extends AppCompatActivity {

  // true = feed, false = tabela
  private boolean viewType;
  private Menu menu;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
    if(preferences.getString(MeuTimeConstants.CURRENT_CLUB_RSS, null) == null) {
      this.setPreferenceClub(MeuTimeConstants.FeedURL.CRUZEIRO, MeuTimeConstants.ClubName.CRUZEIRO);
    }
    this.setViewType(true);

    setContentView(R.layout.activity_main);
    Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
    myToolbar.setBackgroundColor(Color.parseColor("#eaeae5"));
    setSupportActionBar(myToolbar);



    if (savedInstanceState == null) {
      addRssFragment();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    this.setMenu(menu);
    inflater.inflate(R.menu.action_bar, menu);
    return true;
  }

  // Adiciona fragment do feed de notícias
  private void addRssFragment() {
    this.setViewType(false);
    FragmentManager manager = getSupportFragmentManager();
    FragmentTransaction transaction = manager.beginTransaction();
    RssFragment fragment = new RssFragment();
    transaction.replace(R.id.fragment_container, fragment);
    transaction.commit();
  }

  // Adiciona fragment da tabela
  private void addTableFragment() {
    this.setViewType(true);
    FragmentManager manager = getSupportFragmentManager();
    FragmentTransaction transaction = manager.beginTransaction();
    TableFragment fragment = new TableFragment();
    transaction.replace(R.id.fragment_container, fragment);
    transaction.commit();
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putBoolean("fragment_added", true);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.AtleticoMineiro:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.ATLETICO_MG, MeuTimeConstants.ClubName.ATLETICO_MG);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.atleticomg));
        break;
      case R.id.AtleticoParanaense:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.ATLETICO_PR, MeuTimeConstants.ClubName.ATLETICO_PR);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.atleticopr));
        break;
      case R.id.Botafogo:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.BOTAFOGO, MeuTimeConstants.ClubName.BOTAFOGO);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.botafogo));
        break;
      case R.id.Corinthians:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.CORINTHIANS, MeuTimeConstants.ClubName.CORINTHIANS);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.corinthians));
        break;
      case R.id.Coritiba:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.CORITIBA, MeuTimeConstants.ClubName.CORITIBA);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.coritiba));
        break;
      case R.id.Cruzeiro:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.CRUZEIRO, MeuTimeConstants.ClubName.CRUZEIRO);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.drawable.cruzeiro));
        break;
      case R.id.Figueirense:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.FIGUEIRENSE, MeuTimeConstants.ClubName.FIGUEIRENSE);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.figueirense));
        break;
      case R.id.Flamengo:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.FLAMENGO, MeuTimeConstants.ClubName.FLAMENGO);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.flamengo));
        break;
      case R.id.Fluminense:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.FLUMINENSE, MeuTimeConstants.ClubName.FLUMINENSE);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.fluminense));
        break;
      case R.id.Gremio:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.GREMIO, MeuTimeConstants.ClubName.GREMIO);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.gremio));
        break;
      case R.id.Internacional:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.INTERNACIONAL, MeuTimeConstants.ClubName.INTERNACIONAL);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.internacional));
        break;
      case R.id.Palmeiras:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.PALMEIRAS, MeuTimeConstants.ClubName.PALMEIRAS);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.palmeiras));
        break;
      case R.id.SantaCruz:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.SANTA_CRUZ, MeuTimeConstants.ClubName.SANTA_CRUZ);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.santacruz));
        break;
      case R.id.Santos:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.SANTOS, MeuTimeConstants.ClubName.SANTOS);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.santos));
        break;
      case R.id.SaoPaulo:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.SAO_PAULO, MeuTimeConstants.ClubName.SAO_PAULO);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.saopaulo));
        break;
      case R.id.Sport:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.SPORT, MeuTimeConstants.ClubName.SPORT);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.sport));
        break;
      case R.id.Vitória:
        this.setPreferenceClub(MeuTimeConstants.FeedURL.VITORIA, MeuTimeConstants.ClubName.VITORIA);
        this.getMenu().getItem(0).setIcon(getResources().getDrawable(R.mipmap.vitoria));
        break;
      case R.id.changeView:
        //Troca a visão da tela
        Log.d("changeView",String.valueOf(this.isViewType()));
        if(this.isViewType()) {
          this.addRssFragment();
          item.setIcon(getResources().getDrawable(R.mipmap.rsslist));
        } else {
          this.addTableFragment();
          item.setIcon(getResources().getDrawable(R.mipmap.viewlist));
        }
        return true;
      default:
        // Invoke the superclass to handle it.
        return super.onOptionsItemSelected(item);
    }
    this.addRssFragment();
    return true;
  }

  private void setPreferenceClub(String rss, String name) {
    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
    editor.putString(MeuTimeConstants.CURRENT_CLUB_RSS, rss);
    editor.putString(MeuTimeConstants.CURRENT_CLUB, name);
    editor.apply();
  }

  private boolean isViewType() {
    return viewType;
  }

  private void setViewType(boolean viewType) {
    this.viewType = viewType;
  }

  private Menu getMenu() {
    return menu;
  }

  private void setMenu(Menu menu) {
    this.menu = menu;
  }
}

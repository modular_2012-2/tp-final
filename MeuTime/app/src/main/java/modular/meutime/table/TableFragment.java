package modular.meutime.table;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import modular.meutime.R;
import modular.meutime.bean.ClubBean;

/**
 * Classe que gerencia o fragment do feed.
 */
public class TableFragment extends Fragment {

  private ProgressBar progressBar;
  private ListView listViewTable;
  private View view;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    if (view == null) {
      view = inflater.inflate(R.layout.list_fragment_layout, container, false);
    } else {
      ViewGroup parent = (ViewGroup) view.getParent();
      parent.removeView(view);
    }
    progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
    this.listViewTable = (ListView) view.findViewById(R.id.listView);
    startService();
    return view;
  }

  private void startService() {
    Intent intent = new Intent(getActivity(), TableService.class);
    intent.putExtra(TableService.RECEIVER, resultReceiver);
    getActivity().startService(intent);
  }

  /**
   * Recebe o resultado do RSSService
   */
  private final ResultReceiver resultReceiver = new ResultReceiver(new Handler()) {
    @SuppressWarnings("unchecked")
    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      List<ClubBean> items = (List<ClubBean>) resultData.getSerializable(TableService.ITEMS);
      Collections.sort(items);
      if (items != null) {
        TableAdapter adapter = new TableAdapter(items, getActivity());
        listViewTable.setAdapter(adapter);
      } else {
        Toast.makeText(getActivity(), "Erro ao criar a tabela.", Toast.LENGTH_LONG).show();
      }
      // Remove o
      progressBar.setVisibility(View.GONE);
      listViewTable.setVisibility(View.VISIBLE);
    }
  };

}

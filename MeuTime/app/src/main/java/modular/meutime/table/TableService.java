package modular.meutime.table;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.text.ParseException;
import java.util.List;

import modular.meutime.bean.ClubBean;
import modular.meutime.bean.RssBean;
import modular.meutime.parser.ClubParser;
import modular.meutime.service.BaseService;
import modular.meutime.utils.MeuTimeConstants;
import modular.meutime.parser.NewsParser;

/**
 * Serviço para recuperar e exibir a tabela.
 */

public class TableService extends BaseService {

  public static final String ITEMS = "tableItems";
  public static final String RECEIVER = "receiver";

  public TableService() {
    super("TableService");
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    Log.d("MeuTime", "TableService started");
    List<ClubBean> clubItems = null;
    try {
      ClubParser parser = new ClubParser();
      clubItems = parser.parse(getInputStream(MeuTimeConstants.TABLE_API_PATH));
    } catch (Exception e) {
      Log.w(e.getMessage(), e);
    }
    Bundle bundle = new Bundle();
    bundle.putSerializable(ITEMS, (Serializable) clubItems);
    ResultReceiver receiver = intent.getParcelableExtra(RECEIVER);
    receiver.send(0, bundle);
  }

}

package modular.meutime.table;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import modular.meutime.R;
import modular.meutime.bean.ClubBean;
import modular.meutime.bean.RssBean;

/**
 * Adaptador para a tabela de times.
 */

class TableAdapter extends BaseAdapter {

  private final List<ClubBean> items;
  private final Context context;

  public TableAdapter(List<ClubBean> items, Context context) {
    this.items = items;
    this.context = context;
  }

  @Override
  public int getCount() {
    return this.items.size();
  }

  @Override
  public Object getItem(int position) {
    return items.get(position);
  }

  @Override
  public long getItemId(int id) {
    return id;
  }

  @Override
  public View getView(int position, View view, ViewGroup parent) {
    ViewHolder holder = new ViewHolder();
    if (view == null) {
      assert view != null;
      view = View.inflate(context, R.layout.table_item, null);
      holder.clubPos = (TextView) view.findViewById(R.id.clubPos);
      holder.clubName = (TextView) view.findViewById(R.id.clubName);
      holder.clubPoints = (TextView) view.findViewById(R.id.clubPoints);
      holder.clubVictories = (TextView) view.findViewById(R.id.clubVictories);
      holder.clubDraws = (TextView) view.findViewById(R.id.clubDraws);
      holder.clubLosses = (TextView) view.findViewById(R.id.clubLosses);
      holder.clubDifference = (TextView) view.findViewById(R.id.clubDifference);
      view.setTag(holder);
    }
    holder.clubPos.setText(items.get(position).getPositionText());
    holder.clubName.setText(items.get(position).getName());
    holder.clubPoints.setText(items.get(position).getPointsText());
    holder.clubVictories.setText(items.get(position).getVictoriesText());
    holder.clubDraws.setText(items.get(position).getDrawsText());
    holder.clubLosses.setText(items.get(position).getLossesText());
    holder.clubDifference.setText(items.get(position).getGoalsDifferenceText());

    return view;
  }

  static class ViewHolder {
    TextView clubPos;
    TextView clubName;
    TextView clubPoints;
    TextView clubVictories;
    TextView clubDraws;
    TextView clubLosses;
    TextView clubDifference;
  }
}

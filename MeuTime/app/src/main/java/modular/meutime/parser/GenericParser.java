package modular.meutime.parser;

import java.io.InputStream;
import java.util.List;

/**
 * Interface para parsers.
 */

public interface GenericParser {
  /**
   * Faz o parse de um stream de dados
   * @param stream Fluxo de dados
   * @return
     */
  public List<?> parse(InputStream stream) throws Exception;
}

package modular.meutime.parser;

import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import modular.meutime.bean.ClubBean;
import modular.meutime.bean.ClubBuilder;

/**
 * Parser de Clubes - JSON
 */

public class ClubParser implements GenericParser {
  @Override
  public List<ClubBean> parse(InputStream stream) throws Exception {
      BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"), 8);
      StringBuilder stringBuilder = new StringBuilder(stream.available());

      String line;
      while ((line = reader.readLine()) != null) {
        stringBuilder.append(line).append("\n");
      }
      String result = stringBuilder.toString();

      if (stream != null) stream.close();
      return getClubs(result.substring(result.indexOf('{'), result.lastIndexOf('}')+1));
  }

  private List<ClubBean> getClubs(String result) throws Exception {
    List<ClubBean> list = new ArrayList<>();
    JSONObject jObject = new JSONObject(result);
    JSONObject clubNameIds = jObject.getJSONObject("equipes");
    JSONObject clubTable = jObject.getJSONObject("fases").getJSONObject("2357").getJSONObject("classificacao").getJSONObject("equipe");
    Iterator<String> it = clubTable.keys();
    while(it.hasNext()) {
      String key = it.next();
      JSONObject nameId = clubNameIds.getJSONObject(key);
      JSONObject tableEntry = clubTable.getJSONObject(key);
      int pos = tableEntry.getInt("pos");
      Object ganhoPos = tableEntry.get("ganho-pos");
      if(ganhoPos != null && ganhoPos instanceof String && Integer.parseInt((String) ganhoPos) < 0) {
        pos++;
      }
      list.add(new ClubBuilder().name(nameId.getString("nome-comum"))
                                .id(Integer.parseInt(key))
                                .draws(tableEntry.getJSONObject("e").getInt("total"))
                                .losses(tableEntry.getJSONObject("d").getInt("total"))
                                .goalsAgainst(tableEntry.getJSONObject("gc").getInt("total"))
                                .goalsPro(tableEntry.getJSONObject("gp").getInt("total"))
                                .points(tableEntry.getJSONObject("pg").getInt("total"))
                                .position(pos)
                                .victories(tableEntry.getJSONObject("v").getInt("total"))
                                .build());
    }

    return list;
  }
}

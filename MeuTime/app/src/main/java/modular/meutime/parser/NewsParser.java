package modular.meutime.parser;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import modular.meutime.bean.RssBean;
import modular.meutime.utils.MeuTimeConstants;

/**
 * Parser do feed para notícias - XML
 */
public class NewsParser implements GenericParser {

  // We don't use namespaces
  private final String ns = null;

  public List<RssBean> parse(InputStream inputStream) throws XmlPullParserException, IOException, ParseException {
    try {
      XmlPullParser parser = Xml.newPullParser();
      parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
      parser.setInput(inputStream, null);
      parser.nextTag();
      return readFeed(parser);
    } finally {
      inputStream.close();
    }
  }

  private List<RssBean> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException, ParseException {
    parser.require(XmlPullParser.START_TAG, null, "rss");
    String title = null;
    String link = null;
    String description = null;
    Date publishDate = null;
    List<RssBean> items = new ArrayList<>();
    while (parser.next() != XmlPullParser.END_DOCUMENT) {
      if (parser.getEventType() != XmlPullParser.START_TAG) {
        continue;
      }
      String name = parser.getName();
      switch (name) {
        case MeuTimeConstants.FeedTag.TITLE:
          title = read(parser, MeuTimeConstants.FeedTag.TITLE);
          break;
        case MeuTimeConstants.FeedTag.LINK:
          link = read(parser, MeuTimeConstants.FeedTag.LINK);
          break;
        case MeuTimeConstants.FeedTag.DESCRIPTION:
          String desc = read(parser, MeuTimeConstants.FeedTag.DESCRIPTION);
          int index = desc.lastIndexOf('>');
          if (index >= 0) {
            description = desc.substring(index + 1);
          } else {
            description = desc;
          }
          break;
        case MeuTimeConstants.FeedTag.PUBLISH_DATE:
          publishDate = readPublishDate(parser);
          break;
      }
      if (title != null && link != null && description != null && publishDate != null) {
        RssBean bean = new RssBean(title, link, description, publishDate);
        items.add(bean);

        // Evita que informações de um item sejam replicadas a outro
        title = null;
        link = null;
        description = null;
        publishDate = null;
      }
    }
    return items;
  }

  private Date readPublishDate(XmlPullParser parser) throws XmlPullParserException, IOException, ParseException {
    parser.require(XmlPullParser.START_TAG, ns, MeuTimeConstants.FeedTag.PUBLISH_DATE);
    String resultString = getValue(parser);
    parser.require(XmlPullParser.END_TAG, ns, MeuTimeConstants.FeedTag.PUBLISH_DATE);

    // Exemplo - Mon, 12 Dec 2016 18:39:24 -0300
    return new SimpleDateFormat("EEE, FF MMM yyyy HH:mm:ss Z").parse(resultString);

  }

  private String read(XmlPullParser parser, String tagName) throws XmlPullParserException, IOException {
    parser.require(XmlPullParser.START_TAG, ns, tagName);
    String result = getValue(parser);
    parser.require(XmlPullParser.END_TAG, ns, tagName);
    return result;
  }

  private String getValue(XmlPullParser parser) throws XmlPullParserException, IOException {
    String result = "";
    if (parser.next() == XmlPullParser.TEXT) {
      result = parser.getText();
      parser.nextTag();
    }
    return result;
  }
}

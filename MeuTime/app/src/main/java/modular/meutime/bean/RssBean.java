package modular.meutime.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Bean de um item do Feed.
 */

public class RssBean implements Serializable{
  private final String title;
  private final String link;
  private final String description;
  private final Date publishDate;

  // Construtor de um
  public RssBean(String title, String link, String description, Date publishDate) {
    this.title = title;
    this.link = link;
    this.description = description;
    this.publishDate = publishDate;
  }

  public String getTitle() {
    return this.title;
  }

  public String getLink() {
    return this.link;
  }

  public Date getPublishDate() {
    return publishDate;
  }

  public String getDescription() {
    return description;
  }
}

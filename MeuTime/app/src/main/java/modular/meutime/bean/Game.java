package modular.meutime.bean;

/**
 * Bean de jogos de um time.
 */
public class Game {
  private final boolean isHome;
  private final int adversaryId;
  private final int goalsFor;
  private final int goalsAgainst;

  public Game(boolean isHome, int adversaryId, int goalsFor, int goalsAgainst){
    this.goalsAgainst = goalsAgainst;
    this.goalsFor = goalsFor;
    this.isHome = isHome;
    this.adversaryId = adversaryId;
  }

  public boolean isHome() {
    return isHome;
  }

  public int getGoalsFor() {
    return goalsFor;
  }

  public int getGoalsAgainst() {
    return goalsAgainst;
  }

  public int getAdversaryId() {
    return adversaryId;
  }
}

package modular.meutime.bean;

/**
 * Created by Hugo Pimenta on 20/12/2016.
 */

import java.util.List;

/**
 * Builder da Classe de clubes.
 */
public final class ClubBuilder extends ClubBean {
  private final ClubBean club;

  public ClubBuilder(){
    this.club = new ClubBean();
  }
  public ClubBuilder id(int id){
    this.club.id = id;
    return this;
  }
  public ClubBuilder name(String name){
    this.club.name = name;
    return this;
  }
  public ClubBuilder position(int position){
    this.club.position = position;
    return this;
  }
  public ClubBuilder points(int points){
    this.club.points = points;
    return this;
  }
  public ClubBuilder victories(int victories){
    this.club.victories = victories;
    return this;
  }
  public ClubBuilder draws(int draws){
    this.club.draws = draws;
    return this;
  }
  public ClubBuilder losses(int losses){
    this.club.losses = losses;
    return this;
  }
  public ClubBuilder goalsPro(int goalsPro){
    this.club.goalsFor = goalsPro;
    return this;
  }
  public ClubBuilder goalsAgainst(int goalsAgainst){
    this.club.goalsAgainst = goalsAgainst;
    return this;
  }
  public ClubBuilder games(List<Game> games){
    this.club.games = games;
    return this;
  }

  public ClubBean build() {
    return this.club;
  }


}

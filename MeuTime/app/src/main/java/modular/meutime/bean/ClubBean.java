package modular.meutime.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Hugo Pimenta on 20/12/2016.
 */

public class ClubBean implements Serializable, Comparable<ClubBean>{
  int id;
  String name;
  protected int position;
  protected int points;
  protected int victories;
  protected int draws;
  protected int losses;
  protected int goalsFor;
  protected int goalsAgainst;
  protected List<Game> games;

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getPosition() {
    return position;
  }

  public int getPoints() {
    return points;
  }

  public int getVictories() {
    return victories;
  }

  public int getDraws() {
    return draws;
  }

  public int getLosses() {
    return losses;
  }

  public int getGoalsFor() {
    return goalsFor;
  }

  public int getGoalsAgainst() {
    return goalsAgainst;
  }

  public List<Game> getGames() {
    return games;
  }

  public String getPositionText() {
    return String.valueOf(this.position);
  }

  public String getPointsText() {
    return String.valueOf(this.points);
  }

  public String getVictoriesText() {
    return String.valueOf(this.victories);
  }

  public String getDrawsText() {
    return String.valueOf(this.draws);
  }

  public String getLossesText() {
    return String.valueOf(this.losses);
  }

  public String getGoalsDifferenceText() {
    return String.valueOf(this.goalsFor - this.goalsAgainst);
  }

  @Override
  public int compareTo(ClubBean clubBean) {
    return this.position - clubBean.getPosition();
  }
}

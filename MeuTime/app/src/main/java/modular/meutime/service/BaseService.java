package modular.meutime.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Classe abstrata para serviços.
 */
public abstract class BaseService extends IntentService {
  public BaseService(String name) {
    super(name);
  }

  abstract protected void onHandleIntent(Intent intent);
  protected InputStream getInputStream(String link) {
    try {
      URL url = new URL(link);
      return url.openConnection().getInputStream();
    } catch (IOException e) {
      Log.w("MeuTime", "Exception while retrieving the input stream", e);
      return null;
    }
  }

}
